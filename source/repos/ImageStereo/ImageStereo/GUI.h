﻿#pragma once
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include "opencv2/imgproc.hpp"

using namespace System;
using namespace cv;
namespace ImageStereo {

	using namespace System;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Collections::Generic;
	using namespace System::Diagnostics;
	using namespace System::Runtime::InteropServices;
	using namespace System::Drawing::Imaging;
	using namespace System::Threading;	/// <summary>

	/// Summary for GUI
	/// </summary>
	public ref class GUI : public System::Windows::Forms::Form
	{
		ref class Lock {
		public: Object ^ m_pObject;

				Lock(Object^ pObject) : m_pObject(pObject) {
					Monitor::Enter(m_pObject);
				}
				~Lock() {
					Monitor::Exit(m_pObject);
				}
		};
	public: GUI(void)
	{
		InitializeComponent();
		//
		//TODO: Add the constructor code here
		//
	}


			/// <summary>
			/// Clean up any resources being used.
			/// </summary>
	public: ~GUI()
	{
		if (components)
		{
			delete components;
		}
	}
	public:
		System::Windows::Forms::OpenFileDialog^  dlg;
		System::Drawing::Point^ lastPoint = gcnew System::Drawing::Point();
		System::Drawing::Point^ lPoint = gcnew System::Drawing::Point();
		System::Drawing::Point^ recLoc = gcnew System::Drawing::Point();
		System::Drawing::Point^ choosingPoint = gcnew System::Drawing::Point();
		System::Drawing::Graphics^ g2d;
		bool isDrawable;
		System::Drawing::Drawing2D::GraphicsPath^ graphPath = gcnew System::Drawing::Drawing2D::GraphicsPath;
		ArrayList^ X = gcnew ArrayList;
		ArrayList^ Y = gcnew ArrayList;
		System::Drawing::Pen^ pen;
		bool^ leftDown = false;
		bool^ leftup = false;
		System::Drawing::Point cor1, cor2;
		Bitmap^ copiedBitmap;
		bool^ IsCanMove;
		Object^ IsEnableMovingMain = nullptr;

		bool^ isDrag;
		Rectangle theRectangle;
		System::Drawing::Point^ startPoint;

		//int i_maxDisplacement = 12;
		//bool b_Smoothing = true;
		//Bitmap ^ b_Anaglyph;
		//Bitmap ^ b_CH2;
		//Bitmap ^ b_CH1;
		//bool b_SwapRL = false;
		//Bitmap^ image;
		//Bitmap^ depthMap;
		//bool b_InverseDepthMap = false;

	private: System::Windows::Forms::TabPage^  tabPage2;
	public:
	private: System::Windows::Forms::PictureBox^  pAnh1;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  but_stereo;
	private: System::Windows::Forms::Button^  but_anag;
	private: System::Windows::Forms::CheckBox^  cb_inverse;
	private: System::Windows::Forms::CheckBox^  cb_swap;
	private: System::Windows::Forms::CheckBox^  cb_smoothing;
	private: System::Windows::Forms::NumericUpDown^  nud_maxdisp;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::PictureBox^  pKetQua;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::PictureBox^  pAnh2;
	private: System::Windows::Forms::TabPage^  tabPage1;
	public: System::Windows::Forms::SplitContainer^  splitContainer1;
	private:
	public: System::Windows::Forms::Panel^  panel1;
	public: System::Windows::Forms::SplitContainer^  splitContainer2;
	public: System::Windows::Forms::Button^  button4;
	public: System::Windows::Forms::Button^  ChenVung;
	public: System::Windows::Forms::Button^  NapAnhMain;
	public: System::Windows::Forms::Button^  button1;
	public: System::Windows::Forms::PictureBox^  MainImage;
	public: System::Windows::Forms::Panel^  panel2;
	public: System::Windows::Forms::SplitContainer^  splitContainer3;
	public: System::Windows::Forms::Button^  SaoChepVung;
	public: System::Windows::Forms::Button^  button7;
	public: System::Windows::Forms::PictureBox^  SubImage;
	private: System::Windows::Forms::TabControl^  tabControl1;
	public:
	private:
		StereoAlgorithm::StereoAlgorithm ^ ge;
		Object^ result;
	public: System::Windows::Forms::Button^  button5;
	private:

		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// </summary>
		void InitializeComponent(void)
		{
			this->dlg = (gcnew System::Windows::Forms::OpenFileDialog());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->pAnh1 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->but_stereo = (gcnew System::Windows::Forms::Button());
			this->but_anag = (gcnew System::Windows::Forms::Button());
			this->cb_inverse = (gcnew System::Windows::Forms::CheckBox());
			this->cb_swap = (gcnew System::Windows::Forms::CheckBox());
			this->cb_smoothing = (gcnew System::Windows::Forms::CheckBox());
			this->nud_maxdisp = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->pKetQua = (gcnew System::Windows::Forms::PictureBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->pAnh2 = (gcnew System::Windows::Forms::PictureBox());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->splitContainer2 = (gcnew System::Windows::Forms::SplitContainer());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->ChenVung = (gcnew System::Windows::Forms::Button());
			this->NapAnhMain = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->MainImage = (gcnew System::Windows::Forms::PictureBox());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->splitContainer3 = (gcnew System::Windows::Forms::SplitContainer());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->SaoChepVung = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->SubImage = (gcnew System::Windows::Forms::PictureBox());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pAnh1))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nud_maxdisp))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pKetQua))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pAnh2))->BeginInit();
			this->tabPage1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->panel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->BeginInit();
			this->splitContainer2->Panel1->SuspendLayout();
			this->splitContainer2->Panel2->SuspendLayout();
			this->splitContainer2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->MainImage))->BeginInit();
			this->panel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer3))->BeginInit();
			this->splitContainer3->Panel1->SuspendLayout();
			this->splitContainer3->Panel2->SuspendLayout();
			this->splitContainer3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SubImage))->BeginInit();
			this->tabControl1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->pAnh1);
			this->tabPage2->Controls->Add(this->groupBox1);
			this->tabPage2->Controls->Add(this->pKetQua);
			this->tabPage2->Controls->Add(this->button3);
			this->tabPage2->Controls->Add(this->button2);
			this->tabPage2->Controls->Add(this->pAnh2);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(1262, 661);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"DeptMap";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// pAnh1
			// 
			this->pAnh1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->pAnh1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pAnh1->Location = System::Drawing::Point(27, 63);
			this->pAnh1->Name = L"pAnh1";
			this->pAnh1->Size = System::Drawing::Size(300, 247);
			this->pAnh1->TabIndex = 0;
			this->pAnh1->TabStop = false;
			this->pAnh1->Click += gcnew System::EventHandler(this, &GUI::pAnh1_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->Controls->Add(this->button5);
			this->groupBox1->Controls->Add(this->but_stereo);
			this->groupBox1->Controls->Add(this->but_anag);
			this->groupBox1->Controls->Add(this->cb_inverse);
			this->groupBox1->Controls->Add(this->cb_swap);
			this->groupBox1->Controls->Add(this->cb_smoothing);
			this->groupBox1->Controls->Add(this->nud_maxdisp);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(373, 570);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(864, 63);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Cài đặt";
			// 
			// but_stereo
			// 
			this->but_stereo->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->but_stereo->Location = System::Drawing::Point(531, 19);
			this->but_stereo->Name = L"but_stereo";
			this->but_stereo->Size = System::Drawing::Size(109, 23);
			this->but_stereo->TabIndex = 5;
			this->but_stereo->Text = L"Tạo Stereoscopic";
			this->but_stereo->UseVisualStyleBackColor = true;
			this->but_stereo->Click += gcnew System::EventHandler(this, &GUI::but_stereo_Click);
			// 
			// but_anag
			// 
			this->but_anag->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->but_anag->Location = System::Drawing::Point(656, 19);
			this->but_anag->Name = L"but_anag";
			this->but_anag->Size = System::Drawing::Size(98, 23);
			this->but_anag->TabIndex = 6;
			this->but_anag->Text = L"Tạp Anaglyph";
			this->but_anag->UseVisualStyleBackColor = true;
			this->but_anag->Click += gcnew System::EventHandler(this, &GUI::but_anag_Click);
			// 
			// cb_inverse
			// 
			this->cb_inverse->AutoSize = true;
			this->cb_inverse->Location = System::Drawing::Point(399, 23);
			this->cb_inverse->Name = L"cb_inverse";
			this->cb_inverse->Size = System::Drawing::Size(114, 17);
			this->cb_inverse->TabIndex = 4;
			this->cb_inverse->Text = L"Depth Map Ngược";
			this->cb_inverse->UseVisualStyleBackColor = true;
			// 
			// cb_swap
			// 
			this->cb_swap->AutoSize = true;
			this->cb_swap->Location = System::Drawing::Point(266, 23);
			this->cb_swap->Name = L"cb_swap";
			this->cb_swap->Size = System::Drawing::Size(109, 17);
			this->cb_swap->TabIndex = 3;
			this->cb_swap->Text = L"Hướng Phải->Trái";
			this->cb_swap->UseVisualStyleBackColor = true;
			// 
			// cb_smoothing
			// 
			this->cb_smoothing->AutoSize = true;
			this->cb_smoothing->Checked = true;
			this->cb_smoothing->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_smoothing->Location = System::Drawing::Point(172, 23);
			this->cb_smoothing->Name = L"cb_smoothing";
			this->cb_smoothing->Size = System::Drawing::Size(72, 17);
			this->cb_smoothing->TabIndex = 2;
			this->cb_smoothing->Text = L"Làm mượt";
			this->cb_smoothing->UseVisualStyleBackColor = true;
			// 
			// nud_maxdisp
			// 
			this->nud_maxdisp->Location = System::Drawing::Point(79, 22);
			this->nud_maxdisp->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->nud_maxdisp->Name = L"nud_maxdisp";
			this->nud_maxdisp->Size = System::Drawing::Size(78, 20);
			this->nud_maxdisp->TabIndex = 1;
			this->nud_maxdisp->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 12, 0, 0, 0 });
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 24);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(67, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Dịch chuyển";
			// 
			// pKetQua
			// 
			this->pKetQua->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->pKetQua->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pKetQua->Location = System::Drawing::Point(373, 63);
			this->pKetQua->Name = L"pKetQua";
			this->pKetQua->Size = System::Drawing::Size(864, 501);
			this->pKetQua->TabIndex = 4;
			this->pKetQua->TabStop = false;
			this->pKetQua->Click += gcnew System::EventHandler(this, &GUI::pKetQua_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(27, 346);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Nạp ảnh 2";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &GUI::button3_Click_1);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(27, 25);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Nạp ảnh 1";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &GUI::button2_Click_1);
			// 
			// pAnh2
			// 
			this->pAnh2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->pAnh2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pAnh2->Location = System::Drawing::Point(27, 386);
			this->pAnh2->Name = L"pAnh2";
			this->pAnh2->Size = System::Drawing::Size(300, 247);
			this->pAnh2->TabIndex = 1;
			this->pAnh2->TabStop = false;
			this->pAnh2->Click += gcnew System::EventHandler(this, &GUI::pAnh2_Click);
			// 
			// tabPage1
			// 
			this->tabPage1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->tabPage1->Controls->Add(this->splitContainer1);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(1262, 661);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Camera";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// splitContainer1
			// 
			this->splitContainer1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(3, 3);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer1->Panel1->Controls->Add(this->panel1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer1->Panel2->Controls->Add(this->panel2);
			this->splitContainer1->Size = System::Drawing::Size(1254, 653);
			this->splitContainer1->SplitterDistance = 626;
			this->splitContainer1->TabIndex = 1;
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->panel1->Controls->Add(this->splitContainer2);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel1->Location = System::Drawing::Point(0, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(626, 653);
			this->panel1->TabIndex = 1;
			// 
			// splitContainer2
			// 
			this->splitContainer2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer2->Location = System::Drawing::Point(0, 0);
			this->splitContainer2->Name = L"splitContainer2";
			this->splitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this->splitContainer2->Panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer2->Panel1->Controls->Add(this->button4);
			this->splitContainer2->Panel1->Controls->Add(this->ChenVung);
			this->splitContainer2->Panel1->Controls->Add(this->NapAnhMain);
			this->splitContainer2->Panel1->Controls->Add(this->button1);
			// 
			// splitContainer2.Panel2
			// 
			this->splitContainer2->Panel2->Controls->Add(this->MainImage);
			this->splitContainer2->Size = System::Drawing::Size(626, 653);
			this->splitContainer2->SplitterDistance = 75;
			this->splitContainer2->TabIndex = 5;
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::Green;
			this->button4->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button4->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->ForeColor = System::Drawing::Color::White;
			this->button4->Location = System::Drawing::Point(476, 27);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(90, 37);
			this->button4->TabIndex = 3;
			this->button4->Text = L"Lưu Ảnh";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &GUI::button4_Click);
			// 
			// ChenVung
			// 
			this->ChenVung->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->ChenVung->Enabled = false;
			this->ChenVung->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ChenVung->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->ChenVung->ForeColor = System::Drawing::Color::White;
			this->ChenVung->Location = System::Drawing::Point(224, 27);
			this->ChenVung->Name = L"ChenVung";
			this->ChenVung->Size = System::Drawing::Size(90, 37);
			this->ChenVung->TabIndex = 2;
			this->ChenVung->Text = L"Chèn Vùng";
			this->ChenVung->UseVisualStyleBackColor = false;
			this->ChenVung->Click += gcnew System::EventHandler(this, &GUI::ChenVungBtn);
			// 
			// NapAnhMain
			// 
			this->NapAnhMain->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->NapAnhMain->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->NapAnhMain->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->NapAnhMain->ForeColor = System::Drawing::Color::Blue;
			this->NapAnhMain->Location = System::Drawing::Point(137, 27);
			this->NapAnhMain->Name = L"NapAnhMain";
			this->NapAnhMain->Size = System::Drawing::Size(70, 37);
			this->NapAnhMain->TabIndex = 1;
			this->NapAnhMain->Text = L"Nạp Ảnh";
			this->NapAnhMain->UseVisualStyleBackColor = false;
			this->NapAnhMain->Click += gcnew System::EventHandler(this, &GUI::NapAnhMain_Click);
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::White;
			this->button1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->ForeColor = System::Drawing::Color::Blue;
			this->button1->Location = System::Drawing::Point(12, 27);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(104, 37);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Open Webcam";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &GUI::button1_Click);
			// 
			// MainImage
			// 
			this->MainImage->BackColor = System::Drawing::Color::White;
			this->MainImage->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->MainImage->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->MainImage->Dock = System::Windows::Forms::DockStyle::Fill;
			this->MainImage->Location = System::Drawing::Point(0, 0);
			this->MainImage->Name = L"MainImage";
			this->MainImage->Size = System::Drawing::Size(626, 574);
			this->MainImage->TabIndex = 4;
			this->MainImage->TabStop = false;
			this->MainImage->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &GUI::MainImage_Paint);
			this->MainImage->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::MainImage_MouseClick);
			this->MainImage->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::MainImage_MouseDown);
			this->MainImage->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::MainImage_MouseMove);
			this->MainImage->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::MainImage_MouseUp);
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->splitContainer3);
			this->panel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel2->Location = System::Drawing::Point(0, 0);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(624, 653);
			this->panel2->TabIndex = 5;
			// 
			// splitContainer3
			// 
			this->splitContainer3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer3->Location = System::Drawing::Point(0, 0);
			this->splitContainer3->Name = L"splitContainer3";
			this->splitContainer3->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer3.Panel1
			// 
			this->splitContainer3->Panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->splitContainer3->Panel1->Controls->Add(this->SaoChepVung);
			this->splitContainer3->Panel1->Controls->Add(this->button7);
			// 
			// splitContainer3.Panel2
			// 
			this->splitContainer3->Panel2->Controls->Add(this->SubImage);
			this->splitContainer3->Size = System::Drawing::Size(624, 653);
			this->splitContainer3->SplitterDistance = 75;
			this->splitContainer3->TabIndex = 6;
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::Red;
			this->button5->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button5->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button5->ForeColor = System::Drawing::Color::White;
			this->button5->Location = System::Drawing::Point(760, 19);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(90, 23);
			this->button5->TabIndex = 4;
			this->button5->Text = L"Lưu Ảnh";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &GUI::button5_Click_1);
			// 
			// SaoChepVung
			// 
			this->SaoChepVung->BackColor = System::Drawing::Color::Green;
			this->SaoChepVung->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->SaoChepVung->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->SaoChepVung->ForeColor = System::Drawing::Color::White;
			this->SaoChepVung->Location = System::Drawing::Point(187, 27);
			this->SaoChepVung->Name = L"SaoChepVung";
			this->SaoChepVung->Size = System::Drawing::Size(133, 37);
			this->SaoChepVung->TabIndex = 3;
			this->SaoChepVung->Text = L"Sao chép Vùng";
			this->SaoChepVung->UseVisualStyleBackColor = false;
			this->SaoChepVung->Click += gcnew System::EventHandler(this, &GUI::SaoChepVung_Click);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->button7->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button7->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button7->ForeColor = System::Drawing::Color::Blue;
			this->button7->Location = System::Drawing::Point(55, 27);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(90, 37);
			this->button7->TabIndex = 1;
			this->button7->Text = L"Nạp Ảnh";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &GUI::button7_Click);
			// 
			// SubImage
			// 
			this->SubImage->BackColor = System::Drawing::Color::White;
			this->SubImage->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->SubImage->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->SubImage->Dock = System::Windows::Forms::DockStyle::Fill;
			this->SubImage->Location = System::Drawing::Point(0, 0);
			this->SubImage->Name = L"SubImage";
			this->SubImage->Size = System::Drawing::Size(624, 574);
			this->SubImage->TabIndex = 4;
			this->SubImage->TabStop = false;
			this->SubImage->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::SubImage_MouseClick);
			this->SubImage->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &GUI::SubImage_MouseMove);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tabControl1->Location = System::Drawing::Point(0, 0);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(1270, 687);
			this->tabControl1->TabIndex = 2;
			// 
			// GUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::White;
			this->ClientSize = System::Drawing::Size(1270, 687);
			this->Controls->Add(this->tabControl1);
			this->Name = L"GUI";
			this->Text = L"GUI";
			this->Load += gcnew System::EventHandler(this, &GUI::GUI_Load);
			this->tabPage2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pAnh1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->nud_maxdisp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pKetQua))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pAnh2))->EndInit();
			this->tabPage1->ResumeLayout(false);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->splitContainer2->Panel1->ResumeLayout(false);
			this->splitContainer2->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->EndInit();
			this->splitContainer2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->MainImage))->EndInit();
			this->panel2->ResumeLayout(false);
			this->splitContainer3->Panel1->ResumeLayout(false);
			this->splitContainer3->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer3))->EndInit();
			this->splitContainer3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SubImage))->EndInit();
			this->tabControl1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
		System::Void cameraToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			OpenCamera();
		}
		System::Void OpenCamera() {

			cv::VideoCapture Webcam(0);
			if (!Webcam.isOpened()) return;
			int nbImage = 0;
			while (true) {
				cv::Mat frame;
				Webcam >> frame;

				cv::imshow("Webcam", frame);

				int c = cvWaitKey(40);
				if ((static_cast<char>(c) == 's'))
				{
					imwrite(format("C:/Camera/ImageSaved_%d.jpg", nbImage), frame);
					nbImage++;
				}
				if (27 == char(c)) break;
			}
			Webcam.release();
		}
		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			OpenCamera();
		}
		System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {


		}
		System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
			dlg->Title = "Open Image";
			dlg->Filter = "Image File (jpg, jpeg, png)|*.jpg; *.png; *.jpeg";
			dlg->InitialDirectory = "C:\\Download";
			dlg->FileName = "";
			dlg->ShowDialog();
			try
			{
				SubImage->Image = Image::FromStream(dlg->OpenFile());
				SubImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Normal;
				X->Clear();
				Y->Clear();
				MessageBox::Show("Click chuot trai de khoanh vung anh, sau do click chuot phat de hoan thanh!");
				IsEnableMovingMain = nullptr;
			}
			catch (...)
			{
				//do nothing
			}
		}
		System::Void GUI_Load(System::Object^  sender, System::EventArgs^  e) {
			System::IO::Directory::CreateDirectory("C:/Camera");
			System::IO::Directory::CreateDirectory("C:/Download");
			ge = gcnew StereoAlgorithm::StereoAlgorithm();
			ge->AnaglyphComplete += gcnew StereoAlgorithm::StereoAlgorithm::AnaglyphCompleteEventHandler(this, &GUI::AnaglypthFinished);
			ge->StereoscopicComplete += gcnew StereoAlgorithm::StereoAlgorithm::StereoscopicCompleteEventHandler(this, &GUI::StereoscopicFinished);
		}
		void initPaint() {
			g2d = SubImage->CreateGraphics();
			pen = gcnew System::Drawing::Pen(Color::White, 10.0f);
			pen->DashCap = System::Drawing::Drawing2D::DashCap::Round;
			g2d->SmoothingMode = System::Drawing::Drawing2D::SmoothingMode::AntiAlias;
		}
	private: void StereoscopicFinished(bool success, Object^ usercode)
	{
		if (success == false)
		{
			MessageBox::Show("Failed!", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
		else
		{
			pKetQua->Image = ge->Stereoscopic_SideBySide;
			pKetQua->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
		}
	}
	private: void AnaglypthFinished(bool success, Object^ usercode)
	{
		if (success == false)
		{
			MessageBox::Show("Failed!", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
		else
		{
			pKetQua->Image = ge->Anaglyph;
			pKetQua->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
		}
	}
			 //Thuật toán cắt ảnh dưa thừa và xóa phần không được chọn
			 void Algorithm() {
				 //Xoa diem thua
				 //this.removeAll();
				 for (int i = 0; i < X->Count - 1; i++) {
					 for (int j = i + 1; j < X->Count; j++) {
						 if (X[i] == X[j] && Y[i] == Y[j]) {
							 X->Remove(j);
							 Y->Remove(j);
							 j--;
						 }
					 }
				 }
				 int minX = 10000;
				 int minY = 10000;
				 int maxX = -10;
				 int maxY = -10;
				 for (int i = 0; i < X->Count; i++) {

					 if (((int)X[i]) < minX) {
						 minX = ((int)X[i]);
					 }
					 if (((int)Y[i]) < minY) {
						 minY = ((int)Y[i]);
					 }
					 if (((int)X[i]) > maxX) {
						 maxX = ((int)X[i]);
					 }
					 if (((int)Y[i]) > maxY) {
						 maxY = ((int)Y[i]);
					 }
				 }
				 //Tao Anh
				 Bitmap^ subPic = gcnew Bitmap(SubImage->Image);
				 Bitmap^ myBitmap = gcnew Bitmap(subPic->Width, subPic->Height, System::Drawing::Imaging::PixelFormat::Format16bppRgb565);
				 Graphics^ g = Graphics::FromImage(myBitmap);
				 g->FillRectangle(
					 Brushes::White, 0, 0, myBitmap->Width, myBitmap->Height);
				 for (int i = minX; i <= maxX; i++) {
					 for (int j = minY; j <= maxY; j++) {
						 Color^ clr;
						 try
						 {
							 clr = subPic->GetPixel(i, j);
							 int value = clr->A << 24
								 | clr->R << 16
								 | clr->G << 8
								 | clr->B << 0;
							 if (value < -1 || value > 0) {
								 myBitmap->SetPixel(i, j, subPic->GetPixel(i, j));
							 }
						 }
						 catch (...)
						 {
							 //do nothing
						 }
					 }
				 }

				 SubImage->Image = myBitmap;

				 g = Graphics::FromImage(SubImage->Image);
				 g->DrawPath(pen, graphPath);
				 graphPath->Reset();
			 }
			 System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
			 System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

			 }
			 System::Void SubImage_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (e->Button == System::Windows::Forms::MouseButtons::Right)
				 {
					 lastPoint = nullptr;
					 isDrawable = false;
					 Algorithm();
					 return;
				 }
				 if (copiedBitmap != nullptr) return;
				 isDrawable = true;
			 }
			 System::Void SubImage_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (isDrawable == false) {
					 lastPoint = nullptr;
					 return;
				 }
				 if (lastPoint == nullptr) {
					 initPaint();
					 lastPoint = gcnew System::Drawing::Point(e->X, e->Y);
					 X->Add(e->X);
					 Y->Add(e->Y);
					 return;
				 }
				 graphPath->AddLine(lastPoint->X, lastPoint->Y, e->X, e->Y);
				 g2d->DrawPath(pen, graphPath);

				 lastPoint = gcnew System::Drawing::Point(e->X, e->Y);
				 X->Add(e->X);
				 Y->Add(e->Y);
			 }
			 System::Void SaoChepVung_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (SubImage->Image == nullptr) return;
				 //Tao Anh, khởi tạo các giá trị Min, Max để tiền hành cắt khung ảnh cho hợp lý
				 int minX = 10000;
				 int minY = 10000;
				 int maxX = -10;
				 int maxY = -10;
				 Graphics^ g = Graphics::FromImage(SubImage->Image);
				 g->DrawPath(pen, graphPath);
				 graphPath->Reset();

				 Bitmap^ img = gcnew Bitmap(SubImage->Image);
				 for (int i = 0; i < img->Width; i++) {
					 for (int j = 0; j < img->Height; j++) {
						 Color^ c = img->GetPixel(i, j);
						 int clr = c->A << 24
							 | c->R << 16
							 | c->G << 8
							 | c->B << 0;
						 if (clr < -1 || clr > 0) {
							 if (i < minX) {
								 minX = i;
							 }
							 if (j < minY) {
								 minY = j;
							 }
							 if (i > maxX) {
								 maxX = i;
							 }
							 if (j > maxY) {
								 maxY = j;
							 }
						 }
					 }
				 }
				 //Khởi tạo bộ đệm ảnh trong suốt với các kích cỡ, tọa độ vừa tìm được
				 //Vẽ lại
				 Bitmap^ myBitmap = gcnew Bitmap((maxX - minX) + 1, (maxY - minY) + 1, System::Drawing::Imaging::PixelFormat::Format16bppArgb1555);
				 try {
					 for (int i = 0; i < img->Width; i++) {
						 for (int j = 0; j < img->Height; j++) {
							 Color^ c = img->GetPixel(i, j);
							 int clr = c->A << 24
								 | c->R << 16
								 | c->G << 8
								 | c->B << 0;
							 if (clr < -1 || clr > 0) {
								 myBitmap->SetPixel(i - minX, j - minY, img->GetPixel(i, j));
							 }
						 }
					 }
				 }
				 catch (...)
				 {
					 //do nothing
				 }
				 SubImage->Image = myBitmap;
				 copiedBitmap = myBitmap;
				 isDrawable = false;
				 ChenVung->Enabled = true;
				 MessageBox::Show("Sao chep vung anh thanh cong!");
			 }
			 System::Void ChenVungBtn(System::Object^  sender, System::EventArgs^  e) {
				 try
				 {
					 if (copiedBitmap == nullptr) return;
					 MainImage->Image = copiedBitmap;
					 MainImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Normal;
					 MessageBox::Show("Chen vung anh thanh cong. Ban co the di chuyen vung anh bang chot!");
					 IsEnableMovingMain = gcnew System::Drawing::Point();
				 }
				 catch (...)
				 {
					 //do nothing
				 }
			 }
			 System::Void NapAnhMain_Click(System::Object^  sender, System::EventArgs^  e) {
				 dlg->Title = "Open Image";
				 dlg->Filter = "Image File (jpg, jpeg, png)|*.jpg; *.png; *.jpeg";
				 dlg->InitialDirectory = "C:\\Camera";
				 dlg->ShowDialog();
				 try
				 {
					 MainImage->BackgroundImage = Image::FromStream(dlg->OpenFile());
					 MainImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Normal;
				 }
				 catch (...)
				 {
					 //do nothing
				 }
			 }
			 System::Void MainImage_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (IsCanMove && IsEnableMovingMain != nullptr)
				 {
					 recLoc->X = recLoc->X + e->X - choosingPoint->X;
					 recLoc->Y = recLoc->Y + e->Y - choosingPoint->Y;
					 choosingPoint = e->Location;
					 MainImage->Invalidate();
				 }
			 }
			 System::Void MainImage_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (IsEnableMovingMain == nullptr) return;
				 IsCanMove = true;
				 choosingPoint->X = e->X;
				 choosingPoint->Y = e->Y;
				 lPoint->X = e->X;
				 lPoint->Y = e->Y;
			 }
			 System::Void MainImage_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (IsEnableMovingMain == nullptr) return;
				 IsCanMove = false;
				 lPoint->X = e->X;
				 lPoint->Y = e->Y;
			 }
			 System::Void MainImage_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
				 if (IsEnableMovingMain && copiedBitmap != nullptr && recLoc != nullptr) {
					 e->Graphics->DrawImage(MainImage->BackgroundImage, 0, 0);
					 e->Graphics->DrawImage(copiedBitmap, recLoc->X, recLoc->Y);
				 }
			 }

			 System::Void MainImage_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (e->Button == System::Windows::Forms::MouseButtons::Right)
				 {
					 IsEnableMovingMain = nullptr;
					 return;
				 }
				 IsEnableMovingMain = gcnew System::Drawing::Point();
			 }

	private: System::Void button2_Click_1(System::Object^  sender, System::EventArgs^  e) {
		dlg->Title = "Open Image";
		dlg->Filter = "Image File (jpg, jpeg, png)|*.jpg; *.png; *.jpeg";
		dlg->InitialDirectory = "C:\\Camera";
		dlg->ShowDialog();
		try
		{
			pAnh1->Image = Image::FromStream(dlg->OpenFile());
			pAnh1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
		}
		catch (...)
		{
			//do nothing
		}
	}
	private: System::Void button3_Click_1(System::Object^  sender, System::EventArgs^  e) {
		dlg->Title = "Open Image";
		dlg->Filter = "Image File (jpg, jpeg, png)|*.jpg; *.png; *.jpeg";
		dlg->InitialDirectory = "C:\\Camera";
		dlg->ShowDialog();
		try
		{
			pAnh2->Image = Image::FromStream(dlg->OpenFile());
			pAnh2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
		}
		catch (...)
		{
			//do nothing
		}
	}
	private: System::Void but_stereo_Click(System::Object^  sender, System::EventArgs^  e) {
		ge->Smoothing = cb_smoothing->Checked;
		ge->MaxPixelDisplacement = int(nud_maxdisp->Value);
		ge->SwapRightLeft = cb_swap->Checked;
		ge->InverseDepthMap = cb_inverse->Checked;
		ge->GenerateStereoscopicAsync((Bitmap^)pAnh1->Image, (Bitmap^)pAnh2->Image, result);

	}
	private: System::Void pAnh1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void pAnh2_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void pKetQua_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void but_anag_Click(System::Object^  sender, System::EventArgs^  e) {
		ge->Smoothing = cb_smoothing->Checked;
		ge->MaxPixelDisplacement = int(nud_maxdisp->Value);
		ge->SwapRightLeft = cb_swap->Checked;
		ge->InverseDepthMap = cb_inverse->Checked;
		ge->GenerateAnaglyphAsync((Bitmap^)pAnh1->Image, (Bitmap^)pAnh2->Image, result);
	}
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
		Bitmap^ out = gcnew Bitmap(MainImage->BackgroundImage);
		Graphics^ g = Graphics::FromImage(out);
		//g->DrawImage(MainImage->BackgroundImage, 0, 0);
		g->DrawImage(copiedBitmap, recLoc->X, recLoc->Y);
		Random^ rnd = gcnew Random();
		int index = rnd->Next(1000000); // c
		System::String^ path= System::String::Format("C:/Camera/ImageSaved_{0}.jpg", index);
		out->Save(path, ImageFormat::Jpeg);
	}
	private: System::Void button5_Click_1(System::Object^  sender, System::EventArgs^  e) {
		Random^ rnd = gcnew Random();
		int index = rnd->Next(1000000); // c
		System::String^ path = System::String::Format("C:/Camera/KetQua_{0}.jpg", index);
		pKetQua->Image->Save(path, ImageFormat::Jpeg);
	}
};
}
